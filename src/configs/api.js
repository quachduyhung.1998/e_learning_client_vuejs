import localStorageHelper from "@/helper/localStorageHelper";
import axios from "axios";

const instance = axios.create({
    baseURL: process.env.VUE_APP_URL_API,
    timeout: 60 * 1000
});

// Add a request interceptor
instance.interceptors.request.use(
    function(config) {
        // console.log(`Request API ===> ${config.url}`, config.params, config.data);
        return config;
    },
    function(error) {
        return Promise.reject(error);
    }
);

// Add a response interceptor
instance.interceptors.response.use(
    function(response) {
        // console.log(`Response API <=== ${response.config.url}`, response.data);
        // if (response.data.code === 'CURRENT_ROLE_NOT_FOUND') {
        //   logout();
        // }
        return response.data;
    },
    function(error) {
        return Promise.reject(error);
    }
);

async function fetch(url, params = {}, customHeaders = {}) {
    const headers = getHeader(customHeaders);
    return instance.get(url, { params, headers });
}

async function post(url, data = {}, customHeaders = {}) {
    const headers = getHeader(customHeaders);
    return instance.post(url, { ...data }, { headers });
}

async function postFormData(url, data, customHeaders = {}) {
    customHeaders = { ...customHeaders, "Content-Type": "multipart/form-data" };
    const headers = getHeader(customHeaders);
    return instance.post(url, data, { headers });
}

async function put(url, data = {}, customHeaders = {}) {
    const headers = getHeader(customHeaders);
    return instance.put(url, { ...data }, { headers });
}
async function remove(url, data, customHeaders = {}) {
    const headers = getHeader(customHeaders);
    return instance.delete(url, { data, headers });
}

function getHeader(customHeaders) {
    const sessionStr = localStorageHelper.get(
        localStorageHelper.storageKey.TOKEN
    );
    // const currentRole = getRoleByRoute();
    if (sessionStr) {
        return {
            ...customHeaders,
            Authorization: `Bearer ${sessionStr}`
        };
    }

    return { ...customHeaders };
}

const ApiHelper = { fetch, post, postFormData, put, delete: remove };

export default ApiHelper;
