export const SUCCESS = 200;
export const CREATED = 201;
export const BAD_REQUEST = 400;
export const UNAUTHENTICATED = 401;
export const UNAUTHORIZED = 403;
export const NOT_ENOUGH_INFORMATION = 422;
export const PAGE_NOT_FOUND = "PAGE_NOT_FOUND";
