import { reactive, readonly } from "vue";
import localStorageHelper from "@/helper/localStorageHelper";

const state = reactive({
    user: null,
    counter: 0,
    colorCode: "red"
});

const methods = {
    setUser() {
        const user = localStorageHelper.get(
            localStorageHelper.storageKey.USER_SESSION
        );
        state.user = JSON.parse(user);
    },
    decreaseCounter() {
        state.counter--;
    },
    increaseCounter() {
        state.counter++;
    },
    setColorCode(val) {
        state.colorCode = val;
    }
};

const getters = {
    counterSquared() {
        return state.counter * state.counter;
    }
};

// set user
methods.setUser();

export default {
    state: readonly(state),
    methods,
    getters
};
