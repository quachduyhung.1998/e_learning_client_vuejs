/**
 * kiểm tra email
 *
 * @param {string} email
 * @returns
 */
function validateEmail(email) {
    const regex_email = /^(([^<>()[\]\\.,;:\s@\\"]+(\.[^<>()[\]\\.,;:\s@\\"]+)*)|(\\".+\\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    return regex_email.test(String(email).toLowerCase());
}

/**
 * kiểm tra số điện thoại
 *
 * @param {string} phoneNumber
 * @returns
 */
function validatePhone(phoneNumber) {
    const regex_phone = new RegExp(/((09|03|07|08|05)+([0-9]{8})\b)/g);
    return regex_phone.test(phoneNumber);
}

/**
 * giới hạn số lượng chữ hiển thị, mặc định 20 chữ
 *
 * @param {string} text
 * @param {number} limitWord
 * @param {string} addText
 * @returns
 */
function subWord(text, limitWord = 20, addText = "...") {
    if (!text) {
        return "";
    }

    let characters = text.trim().split(" ");
    if (characters.length < limitWord) {
        return text;
    }
    let str = [];
    for (let i = 0; i < characters.length; i++) {
        if (i <= limitWord) {
            str.push(characters[i]);
        }
    }
    return str.join(" ") + addText;
}

/**
 *
 * @param {string} str
 * @returns
 */
function toSlug(str) {
    // Chuyển hết sang chữ thường
    str = str.toLowerCase();

    // xóa dấu
    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, "a");
    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, "e");
    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, "i");
    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, "o");
    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, "u");
    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, "y");
    str = str.replace(/(đ)/g, "d");

    // Xóa ký tự đặc biệt
    str = str.replace(/([^0-9a-z-\s])/g, "");

    // Xóa khoảng trắng thay bằng ký tự -
    str = str.replace(/(\s+)/g, "-");

    // xóa phần dự - ở đầu
    str = str.replace(/^-+/g, "");

    // xóa phần dư - ở cuối
    str = str.replace(/-+$/g, "");

    // return
    return str;
}

function isEmpty(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
}

export default {
    validateEmail,
    validatePhone,
    subWord,
    toSlug,
    isEmpty
};
