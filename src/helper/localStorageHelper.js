const storageKey = {
    TOKEN: "TOKEN",
    USER_SESSION: "USER_SESSION",
    REMEMBER_USER: "REMEMBER_USER",
    CURRENT_ROLE: "CURRENT_ROLE"
};

function save(key, value) {
    localStorage.setItem(key, value);
}

function get(key) {
    return localStorage.getItem(key);
}

function saveObject(key, object) {
    try {
        localStorage.setItem(key, JSON.stringify(object));
        return true;
    } catch (error) {
        return false;
    }
}

function getObject(key) {
    try {
        return JSON.parse(localStorage.getItem(key)?.toString() || "{}");
    } catch (error) {
        return {};
    }
}

function remove(key) {
    localStorage.removeItem(key);
}

function clear() {
    localStorage.clear();
}

function updateUserStorage(user) {
    const sessionStr = localStorage.getItem(storageKey.USER_SESSION);
    if (sessionStr) {
        let session = JSON.parse(sessionStr);
        session = {
            ...session,
            idTokenPayload: user
        };

        localStorage.setItem(storageKey.USER_SESSION, JSON.stringify(session));
    }
}

function updateAccessToken(idToken) {
    const sessionStr = localStorage.getItem(storageKey.USER_SESSION);
    if (sessionStr) {
        let session = JSON.parse(sessionStr);
        session = {
            ...session,
            idToken
        };

        localStorage.setItem(storageKey.USER_SESSION, JSON.stringify(session));
    }
}

function getAccessToken() {
    const sessionStr = localStorage.getItem(storageKey.USER_SESSION) || "";
    if (!sessionStr) return undefined;
    const session = JSON.parse(sessionStr);
    return session.idToken;
}

function getRefreshToken() {
    const sessionStr = localStorage.getItem(storageKey.USER_SESSION) || "";
    if (!sessionStr) return undefined;
    const session = JSON.parse(sessionStr);
    return session.refreshToken;
}

function getUserStorage() {
    const sessionStr = localStorage.getItem(storageKey.USER_SESSION);
    if (!sessionStr) return undefined;
    const session = JSON.parse(sessionStr);
    return session.idTokenPayload;
}

function getCurrentRole() {
    return localStorage.getItem(storageKey.CURRENT_ROLE) || undefined;
}

function clearUserSession() {
    localStorage.removeItem(storageKey.TOKEN);
    localStorage.removeItem(storageKey.USER_SESSION);
}

function getRememberUser() {
    let sessionUser = localStorage.getItem(storageKey.REMEMBER_USER) || "";
    if (!sessionUser) return undefined;
    sessionUser = JSON.parse(sessionUser);
    return sessionUser;
}

function removeRememberUser() {
    localStorage.removeItem(storageKey.REMEMBER_USER);
}

export default {
    storageKey,
    save,
    get,
    saveObject,
    getObject,
    remove,
    clear,
    updateUserStorage,
    getAccessToken,
    getRefreshToken,
    updateAccessToken,
    getUserStorage,
    getCurrentRole,
    clearUserSession,
    getRememberUser,
    removeRememberUser
};
