/**
 * trả về thời gian kiểu string
 * nếu không truyền thời gian vào thì mặc định lấy thời gian hiện tại
 *
 * @param {string} convertTo: Y-m-d H:i:s,...
 * @param {number} timeInt: 1605434599
 * @returns 2020-11-01 17:03:18,...
 */
function getTimeString(convertTo = "Y-m-d H:i:s", timeInt = false) {
    let date = new Date();
    if (timeInt) {
        date = new Date(timeInt * 1000);
    }

    convertTo = convertTo.replace("Y", date.getFullYear());
    convertTo = convertTo.replace("m", ("0" + (date.getMonth() + 1)).slice(-2));
    convertTo = convertTo.replace("d", ("0" + date.getDate()).slice(-2));
    convertTo = convertTo.replace("H", ("0" + date.getHours()).slice(-2));
    convertTo = convertTo.replace("i", ("0" + date.getMinutes()).slice(-2));
    convertTo = convertTo.replace("s", ("0" + date.getSeconds()).slice(-2));
    convertTo = convertTo.replace("w", date.getDay() + 1);

    return convertTo;
}

/**
 * trả về thời gian kiểu number
 * nếu không truyền thời gian vào thì mặc định lấy thời gian hiện tại
 *
 * @param {string} timeString: Y-m-d H:i:s
 * @returns 1605434599
 */
function getTimeInt(timeString = false) {
    let newDate = new Date();
    if (timeString) {
        newDate = new Date(timeString);

        // let date = timeString.split(" ")[0].split("-");
        // let clock = timeString.split(" ")[1].split(":");
        // newDate = new Date(
        //   date[0],
        //   parseInt(date[1]) - 1,
        //   date[2],
        //   clock[0],
        //   clock[1],
        //   clock[2],
        //   0
        // );
    }
    return Math.ceil(newDate.getTime() / 1000);
}

/**
 * trả về thời gian tương tác trước đó dạng string
 *
 * @param {number} time: 1605434599
 * @returns 5 phút trước
 */
function convertTimeAgo(time) {
    let str = "";
    let timeNow = getTimeInt();
    let diff = timeNow - time;
    if (diff < 60) {
        str = "Vừa xong";
    } else if (diff >= 60 && diff < 3600) {
        str = Math.floor(diff / 60) + " phút trước";
    } else if (getTimeString("Y-m-d") == getTimeString("Y-m-d", time)) {
        str = Math.floor(diff / (60 * 24) - 1) + " giờ trước";
    } else if (
        getTimeString("Y-m-d") == getTimeString("Y-m-d", time + 24 * 3600)
    ) {
        str = getTimeString("d/m/Y", time);
    } else if (
        getTimeString("Y-m-d", timeNow - 24 * 3600) >
            getTimeString("Y-m-d", time) &&
        getTimeString("Y-m-d", timeNow - 7 * (24 * 3600)) <
            getTimeString("Y-m-d", time)
    ) {
        let w = getTimeString("w", time);
        let th = "";
        switch (w) {
            case 1:
                th = "T2";
                break;
            case 2:
                th = "T3";
                break;
            case 3:
                th = "T4";
                break;
            case 4:
                th = "T5";
                break;
            case 5:
                th = "T6";
                break;
            case 6:
                th = "T7";
                break;
            default:
                th = "CN";
                break;
        }
        str = th + ", " + getTimeString("H:i", time);
    } else {
        str = getTimeString("H:i d/m/Y", time);
    }
    return str;
}

export default {
    getTimeString,
    getTimeInt,
    convertTimeAgo
};
