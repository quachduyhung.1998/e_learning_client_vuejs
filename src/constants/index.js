export const PUBLIC_LAYOUT = "default";

export const STATUS_NOTIF_UNREAD = 0;
export const STATUS_NOTIF_READED = 1;

export const ROLE_STUDENT = 0;
export const ROLE_TEACHER = 1;
export const ROLE_ADMIN = 2;
export const SELECT_OPTION_ROLE = [
    {
        id: ROLE_STUDENT,
        text: "Học sinh"
    },
    {
        id: ROLE_TEACHER,
        text: "Giáo viên"
    },
    {
        id: ROLE_ADMIN,
        text: "Admin"
    }
];

export const COURSE_CATEGORY_ENGLISH = 1;
export const COURSE_CATEGORY_MATH = 2;
export const SELECT_OPTION_COURSE_CATEGORY = [
    {
        id: 1,
        text: "Tiếng anh"
    },
    {
        id: 2,
        text: "Toán"
    }
];
export const STATUS_COURSE_DRAFT = 0;
export const STATUS_COURSE_COMPLETE = 1;
export const STATUS_COURSE_PUBLIC = 2;
export const TYPE_COURSE_NOT_SEQUENCE = {
    id: 0,
    text: "Không yêu cầu"
};
export const TYPE_COURSE_SEQUENCE = {
    id: 1,
    text: "Yêu cầu"
};

export const TYPE_LESSON_NORMAL = 0;
export const TYPE_LESSON_VIDEO = 1;
export const SELECT_OPTION_TYPE_LESSON = [
    {
        id: 0,
        text: "Bài học dạng chữ"
    },
    {
        id: 1,
        text: "Bài học dạng video"
    }
];

export const QUIZ_NOT_CHECKED = 0;
export const QUIZ_CHECKED = 1;
export const TYPE_QUIZ_ONCE_RIGHT_ANSWER = 1;
export const TYPE_QUIZ_MULTI_RIGHT_ANSWER = 2;
export const TYPE_QUIZ_TRUE_FALSE_ANSWER = 3;
export const SELECT_TYPE_QUIZ = [
    {
        id: 1,
        text: "Chọn 1 đáp án"
    },
    {
        id: 2,
        text: "Chọn nhiều đáp án"
    },
    {
        id: 3,
        text: "Đúng/Sai"
    }
];
