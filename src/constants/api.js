export default {
    LOGIN: "/login",
    LOGOUT: "/logout",
    // notification
    GET_LIST_NOTIFICATION: "/notifications/get-all",
    // chat
    // user
    GET_LIST_USER: "/users"
};
