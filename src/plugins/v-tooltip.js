import tooltipDirective from "./v-tooltip.js";

// register all directives
const directives = app => {
    tooltipDirective(app);
};

export default directives;
