import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { registerGlobalComponents } from "./configs/import";
import directives from "./plugins";

// import "./assets/styles/reset.css";
import "./assets/styles/tailwind.css";
import "mosha-vue-toastify/dist/style.css";
import "./assets/styles/global.scss";

const app = createApp(App);
registerGlobalComponents(app);
directives(app); // tooltip
app.use(router);
app.mount("#app");
