import { ROLE_ADMIN } from "@/constants";

export default function routeAdmin(auth) {
    return [
        {
            path: "/admin",
            name: "admin",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin" */ "@/pages/admin/dashboard"
                )
        },
        {
            path: "/admin/users",
            name: "admin-user",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-user" */ "@/pages/admin/user"
                )
        },
        {
            path: "/admin/users/:userId",
            name: "admin-user-view",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-user-view" */ "@/pages/admin/user/view.vue"
                )
        },
        {
            path: "/admin/users/create",
            name: "admin-user-create",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-user-create" */ "@/pages/admin/user/create.vue"
                )
        },
        {
            path: "/admin/users/update/:userId",
            name: "admin-user-update",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-user-update" */ "@/pages/admin/user/update.vue"
                )
        },
        {
            path: "/admin/courses",
            name: "admin-course",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-course" */ "@/pages/admin/course"
                )
        },
        {
            path: "/admin/courses/:courseId",
            name: "admin-course-view",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-course-view" */ "@/pages/admin/course/view.vue"
                )
        },
        {
            path: "/admin/courses/:courseId/lessons/:lessonId",
            name: "admin-lesson-view",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-lesson-view" */ "@/pages/admin/lesson/view.vue"
                )
        },
        {
            path: "/admin/courses/:courseId/tests/:testId",
            name: "admin-test-view",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-test-view" */ "@/pages/admin/test/view.vue"
                )
        },
        {
            path: "/admin/classes",
            name: "admin-class",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-class" */ "@/pages/admin/class"
                )
        },
        {
            path: "/admin/classes/:classId",
            name: "admin-class-view",
            meta: {
                middleware: [ROLE_ADMIN]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "admin-class-view" */ "@/pages/admin/class/view.vue"
                )
        }
    ];
}
