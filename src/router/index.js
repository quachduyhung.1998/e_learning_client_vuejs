import { createRouter, createWebHistory } from "vue-router";
import routeAdmin from "./admin";
import routeTeacher from "./teacher";
import routeStudent from "./student";
import auth from "@/middleware/auth";

const routes = [
    ...routeAdmin(auth),
    ...routeTeacher(auth),
    ...routeStudent(auth),

    /********************** route chung ************************/
    {
        path: "/login",
        name: "login",
        meta: {
            layout: "auth"
        },
        beforeEnter: auth,
        component: () =>
            import(/* webpackChunkName: "login" */ "@/pages/auth/login")
    },
    {
        path: "/logout",
        name: "logout",
        beforeEnter: auth,
        component: () =>
            import(/* webpackChunkName: "logout" */ "@/pages/auth/logout")
    },
    {
        path: "/profile",
        name: "profile",
        beforeEnter: auth,
        component: () =>
            import(/* webpackChunkName: "profile" */ "@/pages/user/profile")
    },
    {
        path: "/test",
        name: "test",
        beforeEnter: auth,
        component: () => import(/* webpackChunkName: "test" */ "@/pages/test")
    },
    {
        path: "/:patchMatch(.*)*",
        name: "not-found",
        beforeEnter: auth,
        component: () =>
            import(/* webpackChunkName: "notfound" */ "@/pages/error/404.vue")
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

export default router;
