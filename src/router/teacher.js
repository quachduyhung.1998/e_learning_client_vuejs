import { ROLE_TEACHER } from "@/constants";

export default function routeTeacher(auth) {
    return [
        {
            path: "/teacher",
            name: "teacher",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher" */ "@/pages/teacher/dashboard"
                )
        },
        {
            path: "/teacher/courses",
            name: "teacher-course",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-course" */ "@/pages/teacher/course"
                )
        },
        {
            path: "/teacher/courses/:courseId",
            name: "teacher-course-view",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-course-view" */ "@/pages/teacher/course/view.vue"
                )
        },
        {
            path: "/teacher/courses/create",
            name: "teacher-course-create",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-course-create" */ "@/pages/teacher/course/create.vue"
                )
        },
        {
            path: "/teacher/courses/update/:courseId",
            name: "teacher-course-update",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-course-update" */ "@/pages/teacher/course/update.vue"
                )
        },
        {
            path: "/teacher/courses/:courseId/lessons/create",
            name: "teacher-lesson-create",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-lesson-create" */ "@/pages/teacher/lesson/create.vue"
                )
        },
        {
            path: "/teacher/courses/:courseId/lessons/:lessonId",
            name: "teacher-lesson-update",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-lesson-update" */ "@/pages/teacher/lesson/update.vue"
                )
        },
        {
            path: "/teacher/question-bank",
            name: "teacher-quiz",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-quiz" */ "@/pages/teacher/quiz"
                )
        },
        {
            path: "/teacher/question-bank/create",
            name: "teacher-quiz-create",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-quiz-create" */ "@/pages/teacher/quiz/create.vue"
                )
        },
        {
            path: "/teacher/question-bank/:quizId",
            name: "teacher-quiz-update",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-quiz-update" */ "@/pages/teacher/quiz/update.vue"
                )
        },
        {
            path: "/teacher/courses/:courseId/tests/create",
            name: "teacher-test-create",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-quiz-create" */ "@/pages/teacher/test/create.vue"
                )
        },
        {
            path: "/teacher/courses/:courseId/tests/:testId",
            name: "teacher-test-update",
            meta: {
                middleware: [ROLE_TEACHER]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "teacher-quiz-update" */ "@/pages/teacher/test/update.vue"
                )
        }
    ];
}
