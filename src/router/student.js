import { ROLE_STUDENT } from "@/constants";

export default function routeStudent(auth) {
    return [
        {
            path: "/",
            name: "student",
            meta: {
                middleware: [ROLE_STUDENT]
            },
            beforeEnter: auth,
            component: () =>
                import(
                    /* webpackChunkName: "student" */ "@/pages/student/dashboard"
                )
        }
    ];
}
