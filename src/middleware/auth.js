import localStorageHelper from "@/helper/localStorageHelper";
import checkRole from "./checkRole";

export default function auth(to, from, next) {
    const userCurrent = JSON.parse(
        localStorageHelper.get(localStorageHelper.storageKey.USER_SESSION)
    );

    // check to route login
    if (to.name === "login") {
        if (userCurrent) next({ name: from.name, params: {} });
        else next();
    } else {
        // check login
        if (!userCurrent) next({ name: "login", params: {} });
        else {
            checkRole(to, next, userCurrent.role);
        }
    }
}
