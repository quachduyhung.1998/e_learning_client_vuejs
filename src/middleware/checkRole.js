import { ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT } from "@/constants";

export default function checkRole(to, next, roleCurrent) {
    const arrayMiddleware = to.meta.middleware;
    if (arrayMiddleware && Array.isArray(arrayMiddleware)) {
        if (arrayMiddleware.includes(roleCurrent)) {
            next();
        } else {
            if (roleCurrent === ROLE_ADMIN) {
                next({ name: "admin" });
            } else if (roleCurrent === ROLE_TEACHER) {
                next({ name: "teacher" });
            } else if (roleCurrent === ROLE_STUDENT) {
                next({ name: "student" });
            }
        }
    } else {
        next();
    }
}
