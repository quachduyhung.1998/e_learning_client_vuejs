import { ref } from "vue";

const error = ref(null);
const isLoading = ref(null);

export default {
    error,
    isLoading
};
