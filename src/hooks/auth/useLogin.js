import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import {
    UNAUTHENTICATED,
    NOT_ENOUGH_INFORMATION
} from "@/configs/responseCode";

const errorResponse = ref(null);
const isLoading = ref(false);

async function login(email, password, rememberUser = false) {
    errorResponse.value = null;
    isLoading.value = true;
    try {
        const data = { email, password };
        const response = await ApiHelper.post("/login", data);
        onLogin(response, rememberUser, data);
    } catch (error) {
        if (error.response.status === UNAUTHENTICATED)
            errorResponse.value = "Sai thông tin đăng nhập.";
        if (error.response.status === NOT_ENOUGH_INFORMATION)
            errorResponse.value = "Bạn chưa nhập đầy đủ thông tin.";
    } finally {
        isLoading.value = false;
    }
}

function onLogin(response, rememberUser, data) {
    // nhớ tài khoản
    if (rememberUser) {
        localStorageHelper.saveObject(
            localStorageHelper.storageKey.REMEMBER_USER,
            data
        );
    } else {
        localStorageHelper.removeRememberUser();
    }

    // lưu token
    localStorageHelper.save(
        localStorageHelper.storageKey.TOKEN,
        response.access_token
    );

    // lưu user session
    localStorageHelper.saveObject(
        localStorageHelper.storageKey.USER_SESSION,
        response.user
    );
}

export function useLogin() {
    return { errorResponse, isLoading, login };
}
