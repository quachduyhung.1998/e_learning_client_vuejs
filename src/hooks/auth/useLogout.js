import { useRouter } from "vue-router";
// import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useLogout() {
    const router = useRouter();

    async function logout() {
        try {
            // await ApiHelper.post("/logout");

            // logout
            localStorageHelper.clearUserSession();

            router.push({ name: "login", params: {} });
        } catch (err) {
            if (err.response.status === UNAUTHENTICATED) {
                // logout
                localStorageHelper.clearUserSession();
            }
            console.log(err);
        }
    }

    return { logout };
}
