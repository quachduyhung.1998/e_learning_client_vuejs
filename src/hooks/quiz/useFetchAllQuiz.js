import { ref } from "vue";
import ApiHelper from "@/configs/api";
import { UNAUTHENTICATED } from "@/configs/responseCode";
import localStorageHelper from "@/helper/localStorageHelper";
import { COURSE_CATEGORY_ENGLISH } from "@/constants";

export default function useFetchAllQuiz() {
    const errorResponse = ref(null);
    const isLoading = ref(false);
    const listQuiz = ref([]);

    async function onFetchAllQuiz(
        course_category_id = COURSE_CATEGORY_ENGLISH,
        params = null
    ) {
        errorResponse.value = null;
        isLoading.value = true;
        try {
            if (!params) {
                params = { name: null, page: 1 };
            }
            const response = await ApiHelper.fetch(
                `/course-category/${course_category_id}/get-all-question`,
                params
            );
            listQuiz.value = response.data;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            console.log(error);
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponse, isLoading, listQuiz, onFetchAllQuiz };
}
