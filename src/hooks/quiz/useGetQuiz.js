import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useGetQuiz() {
    const errorResponse = ref(null);
    const isLoading = ref(false);
    const quiz = ref(null);

    async function onGetQuiz(id) {
        errorResponse.value = null;
        isLoading.value = true;
        try {
            const response = await ApiHelper.fetch(`/questions/${id}`);
            quiz.value = response.data;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            console.log(error);
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponse, isLoading, quiz, onGetQuiz };
}
