import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useCreateClass() {
    const errorResponse = ref(null);
    const isLoadingCreateClass = ref(false);
    const newClass = ref(null);

    async function onCreateClass(name, userId) {
        errorResponse.value = null;
        isLoadingCreateClass.value = true;
        try {
            const data = {
                name,
                user_id: userId
            };
            const response = await ApiHelper.post("/classes", data);
            newClass.value = response.data;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            errorResponse.value = error.response.data.errors;
        } finally {
            isLoadingCreateClass.value = false;
        }
    }

    return { errorResponse, isLoadingCreateClass, newClass, onCreateClass };
}
