import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useUpdateClass() {
    const errorResponseUpdate = ref(null);
    const isLoadingUpdateClass = ref(false);
    const classUpdated = ref(null);

    async function onUpdateClass(id, name, userId) {
        errorResponseUpdate.value = null;
        isLoadingUpdateClass.value = true;
        try {
            const data = {
                name,
                user_id: userId
            };
            const response = await ApiHelper.put(`/classes/${id}`, data);
            classUpdated.value = response.data;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            errorResponseUpdate.value = error.response.data.errors;
        } finally {
            isLoadingUpdateClass.value = false;
        }
    }

    return {
        errorResponseUpdate,
        isLoadingUpdateClass,
        classUpdated,
        onUpdateClass
    };
}
