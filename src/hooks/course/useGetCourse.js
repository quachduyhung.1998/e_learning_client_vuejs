import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useGetCourse() {
    const errorResponse = ref(null);
    const isLoading = ref(false);
    const course = ref(null);

    async function onGetCourse(id) {
        errorResponse.value = null;
        isLoading.value = true;
        try {
            const response = await ApiHelper.fetch(`/courses/${id}`);
            course.value = response.data;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            console.log(error);
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponse, isLoading, course, onGetCourse };
}
