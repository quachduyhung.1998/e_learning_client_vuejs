import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useUpdateStatusCompleteCourse() {
    const errorResponseUpdate = ref(null);
    const isLoadingUpdate = ref(false);

    async function onUpdateStatusCompleteCourse(id) {
        errorResponseUpdate.value = null;
        isLoadingUpdate.value = true;
        try {
            await ApiHelper.put(`/courses/${id}/complete`);
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            errorResponseUpdate.value = error.response.data.errors;
        } finally {
            isLoadingUpdate.value = false;
        }
    }

    return {
        errorResponseUpdate,
        isLoadingUpdate,
        onUpdateStatusCompleteCourse
    };
}
