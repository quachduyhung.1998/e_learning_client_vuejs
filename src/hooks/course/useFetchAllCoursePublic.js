import { ref } from "vue";
import ApiHelper from "@/configs/api";
import { UNAUTHENTICATED } from "@/configs/responseCode";
import localStorageHelper from "@/helper/localStorageHelper";

export default function useFetchAllCoursePublic() {
    const errorResponse = ref(null);
    const isLoading = ref(false);
    const listCourse = ref([]);

    async function onFetchAllCoursePublic(params = null) {
        errorResponse.value = null;
        isLoading.value = true;
        try {
            if (!params) {
                params = { name: null, category: null, page: 1 };
            }
            const response = await ApiHelper.fetch(`/courses`, params);
            listCourse.value = response.data;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            console.log(error);
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponse, isLoading, listCourse, onFetchAllCoursePublic };
}
