import { ref } from "vue";
import ApiHelper from "@/configs/api";
import { UNAUTHENTICATED } from "@/configs/responseCode";
import localStorageHelper from "@/helper/localStorageHelper";

export default function useDeleteCourse() {
    const errorResponse = ref(null);
    const isLoading = ref(false);

    async function onDeleteCourse(id) {
        errorResponse.value = null;
        isLoading.value = true;
        try {
            await ApiHelper.delete(`/courses/${id}`);
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            errorResponse.value = error.response.data.errors;
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponse, isLoading, onDeleteCourse };
}
