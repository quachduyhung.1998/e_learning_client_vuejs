import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED, UNAUTHORIZED } from "@/configs/responseCode";

export default function useCreateTeacherApprovalCourse() {
    const errorResponse = ref(null);
    const isLoadingCreateTeacherApproval = ref(false);

    async function onCreateTeacherApproval(userId, courseId) {
        errorResponse.value = null;
        isLoadingCreateTeacherApproval.value = true;
        try {
            const data = {
                user_id: userId,
                course_id: courseId
            };
            await ApiHelper.post("/teacher-approval-course", data);
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            } else if (error.response.status === UNAUTHORIZED) {
                errorResponse.value = error.response.data.message;
            } else {
                errorResponse.value = error.response.data.errors;
            }
        } finally {
            isLoadingCreateTeacherApproval.value = false;
        }
    }

    return {
        errorResponse,
        isLoadingCreateTeacherApproval,
        onCreateTeacherApproval
    };
}
