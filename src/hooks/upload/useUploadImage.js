import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useUploadImage() {
    const errorResponse = ref(null);
    const isLoading = ref(false);
    const linkImage = ref(null);

    async function onUpload(fromData) {
        errorResponse.value = null;
        isLoading.value = true;
        try {
            const response = await ApiHelper.postFormData(
                "/upload-image",
                fromData
            );
            linkImage.value = response.data.url;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            errorResponse.value = error.response.data.errors;
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponse, isLoading, linkImage, onUpload };
}
