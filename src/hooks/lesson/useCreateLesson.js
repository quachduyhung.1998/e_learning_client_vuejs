import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useCreateLesson() {
    const errorResponse = ref(null);
    const isLoading = ref(false);

    async function onCreateLesson(data) {
        errorResponse.value = null;
        isLoading.value = true;
        try {
            await ApiHelper.post("/lessons", data);
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            errorResponse.value = error.response.data.errors;
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponse, isLoading, onCreateLesson };
}
