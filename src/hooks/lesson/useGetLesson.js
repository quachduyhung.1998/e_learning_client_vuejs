import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useGetLesson() {
    const errorResponse = ref(null);
    const isLoadingLesson = ref(false);
    const lesson = ref(null);

    async function onGetLesson(id) {
        errorResponse.value = null;
        isLoadingLesson.value = true;
        try {
            const response = await ApiHelper.fetch(`/lessons/${id}`);
            lesson.value = response.data;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            console.log(error);
        } finally {
            isLoadingLesson.value = false;
        }
    }

    return { errorResponse, isLoadingLesson, lesson, onGetLesson };
}
