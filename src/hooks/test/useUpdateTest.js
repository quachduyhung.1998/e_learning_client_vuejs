import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useUpdateTest() {
    const errorResponseUpdate = ref(null);
    const isLoading = ref(false);

    async function onUpdateTest(id, data) {
        errorResponseUpdate.value = null;
        isLoading.value = true;
        try {
            await ApiHelper.put(`/tests/${id}`, data);
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            errorResponseUpdate.value = error.response.data.errors;
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponseUpdate, isLoading, onUpdateTest };
}
