import { ref } from "vue";
import ApiHelper from "@/configs/api";
import { UNAUTHENTICATED, UNAUTHORIZED } from "@/configs/responseCode";
import localStorageHelper from "@/helper/localStorageHelper";

export default function useDeleteTest() {
    const errorResponseTest = ref(null);
    const isLoading = ref(false);

    async function onDeleteTest(id) {
        errorResponseTest.value = null;
        isLoading.value = true;
        try {
            await ApiHelper.delete(`/tests/${id}`);
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            } else if (error.response.status === UNAUTHORIZED) {
                errorResponseTest.value = error.response.data.message;
            } else {
                errorResponseTest.value = error.response.data.errors;
            }
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponseTest, isLoading, onDeleteTest };
}
