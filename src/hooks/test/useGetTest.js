import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useGetTest() {
    const errorResponse = ref(null);
    const isLoadingTest = ref(false);
    const test = ref(null);

    async function onGetTest(id) {
        errorResponse.value = null;
        isLoadingTest.value = true;
        try {
            const response = await ApiHelper.fetch(`/tests/${id}`);
            test.value = response.data;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            console.log(error);
        } finally {
            isLoadingTest.value = false;
        }
    }

    return { errorResponse, isLoadingTest, test, onGetTest };
}
