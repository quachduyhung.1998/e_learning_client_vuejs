import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useFetchAllNotification() {
    const isLoading = ref(false);
    const listNotification = ref(null);

    async function onFetchAllNotification(page = 1) {
        if (page === 1) {
            isLoading.value = true;
        }
        try {
            let params = { page };
            const response = await ApiHelper.fetch(
                "/notifications/get-all",
                params
            );
            listNotification.value = response.data;
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            console.log(error);
        } finally {
            isLoading.value = false;
        }
    }

    return { isLoading, listNotification, onFetchAllNotification };
}
