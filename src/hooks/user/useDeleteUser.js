import { ref } from "vue";
import ApiHelper from "@/configs/api";
import { UNAUTHENTICATED } from "@/configs/responseCode";
import localStorageHelper from "@/helper/localStorageHelper";

export default function useDeleteUser() {
    const errorResponse = ref(null);
    const isLoading = ref(false);

    async function onDeleteUser(id) {
        errorResponse.value = null;
        isLoading.value = true;
        try {
            await ApiHelper.delete(`/users/${id}`);
        } catch (error) {
            if (error.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            }
            errorResponse.value = error.response.data.errors;
        } finally {
            isLoading.value = false;
        }
    }

    return { errorResponse, isLoading, onDeleteUser };
}
