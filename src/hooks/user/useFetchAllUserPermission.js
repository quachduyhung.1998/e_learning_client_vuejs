import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useFetchAllUserPermission() {
    const isLoading = ref(false);
    const listUserWithPermission = ref(null);

    async function onFetchAllUserPermission(permission) {
        isLoading.value = true;
        try {
            const response = await ApiHelper.fetch(
                `/users/permissions/${permission}`
            );

            listUserWithPermission.value = response.data;
        } catch (err) {
            if (err.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            } else {
                console.log(err);
            }
        } finally {
            isLoading.value = false;
        }
    }

    return { isLoading, listUserWithPermission, onFetchAllUserPermission };
}
