import { ref } from "vue";
import ApiHelper from "@/configs/api";
import localStorageHelper from "@/helper/localStorageHelper";
import { UNAUTHENTICATED } from "@/configs/responseCode";

export default function useFetchAllUser() {
    const isLoading = ref(false);
    const listUser = ref(null);

    async function onFetchAllUser(params = null) {
        isLoading.value = true;
        try {
            if (!params) {
                params = { keyword: null, role: null, page: 1 };
            }
            const response = await ApiHelper.fetch("/users", params);

            listUser.value = response.data;
        } catch (err) {
            if (err.response.status === UNAUTHENTICATED) {
                localStorageHelper.clearUserSession();
            } else {
                console.log(err);
            }
        } finally {
            isLoading.value = false;
        }
    }

    return { isLoading, listUser, onFetchAllUser };
}
