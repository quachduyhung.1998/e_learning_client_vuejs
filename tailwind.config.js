module.exports = {
    purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                dark: "#363853",
                // "main-color": "#2d9fdc"
                "main-color": "#7367F0",
                "main-color-active-text": "#6a5fdd",
                "main-color-hover-header-1": "rgba(115,103,240,.12)",
                "main-color-hover-header-2": "#f8f8f8",
                "color-primary": "#7367F0",
                "color-secondary": "#82868B",
                "color-danger": "#EA5455",
                "color-success": "#28C76F",
                "color-warning": "#FF9F43",
                "color-info": "#00CFE8"
            },
            zIndex: {
                "-1": -1,
                "0": 0,
                "1": 1,
                "10": 10,
                "9": 9,
                "20": 20,
                "25": 25,
                "30": 30,
                "40": 40,
                "50": 50,
                "75": 75,
                "99": 99,
                "100": 100,
                "999": 999,
                "9999": 9999,
                "99999": 99999,
                auto: "auto"
            },
            boxShadow: {
                sm: "0 1px 2px 0 rgba(0, 0, 0, 0.05)",
                DEFAULT:
                    "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
                md:
                    "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
                lg:
                    "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)",
                xl:
                    "0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)",
                "2xl": "0 25px 50px -12px rgba(0, 0, 0, 0.25)",
                "3xl": "0 35px 60px -15px rgba(0, 0, 0, 0.3)",
                inner: "inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)",
                none: "none",
                header: "0 4px 24px 0 rgba(34, 41, 47, 0.1)",
                card: "0 4px 24px 0 rgba(34, 41, 47, 0.1)",
                sidebar: "0 0 15px 0 rgba(34, 41, 47, 0.05)",
                "hover-primary": "0 8px 25px -8px #7367f0",
                "hover-secondary": "0 8px 25px -8px #82868B",
                "hover-danger": "0 8px 25px -8px #EA5455",
                "hover-success": "0 8px 25px -8px #28C76F",
                "hover-warning": "0 8px 25px -8px #FF9F43",
                "hover-info": "0 8px 25px -8px #00CFE8"
            },
            maxHeight: {
                "box-header": "500px"
            },
            width: {
                400: "400px"
            },
            minWidth: {
                28: "112px"
            },
            padding: {
                21: "84px"
            }
        }
    },
    variants: {
        extend: {}
    },
    plugins: []
};
